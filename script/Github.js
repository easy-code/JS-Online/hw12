class Github {
    constructor() {
        this.client_id = "df718fa3fd229b83027f";
        this.client_secret = "dea9e5101bbce9521d3767e6bbe3647a09d59a5b";
    }

    // Get user by name
    getUser(name) {
        return new Promise((resolve, reject) => {
            fetch(`https://api.github.com/users/${name}?client_id=${this.client_id}&client_secret=${this.client_secret}`)
                .then(res => res.json())
                .then(user => resolve(user))
                .catch(err => reject(err))
        })
    }

    getRepos(user) {
        return new Promise((resolve, reject) => {
            if (!user.login) reject('User not found');

            fetch(`https://api.github.com/users/${user.login}/repos?per_page=${5}&sort=${'created: asc'}&client_id=${this.client_id}&client_secret=${this.client_secret}`)
                .then(res => res.json())
                .then(user => resolve(user))
                .catch(err => reject(err))
        })
    }
}
