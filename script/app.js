// Init Github
const gitHub = new Github();

// Init UI
const ui = new UI();

// Init search input
const searchInput = document.getElementById('searchUser');

// Add Event Listener
searchInput.addEventListener('keyup', (e) => {
    // Get input text
    const userText = e.target.value;

    ui.showPreload();

    if (userText !== '') {
        // Make http request
        gitHub.getUser(userText)
            .then(user => {
                if (user.message === 'Not Found') {
                    // Show alert
                    ui.showAlert(`User: ${userText} not found`, 'alert alert-danger');
                    ui.clearProfile();
                } else {
                    // Show profile
                    ui.showProfile(user);
                    ui.clearAlert()
                }

                return user;
            })
            .then(user => gitHub.getRepos(user))
            .then(repos => {
                ui.showRepos(repos);
            })
            .catch(err => console.log(err));
    } else {
        // Clear profile
        ui.clearProfile();
    }
});
