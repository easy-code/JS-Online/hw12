##### Cсылка на видео
[Video](https://drive.google.com/open?id=1LFEZblqzqgHn01zaPH_hdyhEPXDlAU3g)

##### Ссылки на презентацию
[First](https://drive.google.com/file/d/0B061Zp6B-nxPTGhDT081cFEwcnc/view)

[Second](https://docs.google.com/presentation/d/1gfk2YaP6bIIeVvL5tgiYTOYoujuJrA7ioQUOrzo1gxc/edit#slide=id.p)

##### Справочники
[developer.mozilla.org](https://developer.mozilla.org/uk/docs/Web/JavaScript)

[learn.javascript.ru](https://learn.javascript.ru/getting-started)

##### Стиль кода
https://learn.javascript.ru/coding-style
